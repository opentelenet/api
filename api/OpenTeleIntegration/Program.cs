﻿/*
 * Copyright 2015 the original authors Stefan Wagner 
 * Contact sw@eng.au.dk for additional information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Introduction (Author Stefan Wagner):
 * This is the first version of a C# based .NET wrapper for the OpenTele2 web API.
 * Focus is on the patient API, but work is also commencing on wrapping the
 * clinical interface. The patient API allows for getting and sending questionnaire data belonging to a patient:
 * Questionnaire data, automated or manual medical device measurements, and subjective observations, 
 * as well as for exchanging text messages between the clinicians. 
 * The OpenTele project is made to promote the use of telemedicine, by illustrating how monitoring and training
 * can happen remotely. It is part of the national Danish strategy for telemedicine, and it is being used in
 * such diverse areas as pre-eclampsia monitoring, COPD, diabets, and patients suffering from depression 
 * and dementia. This .NET API has been created to support moving to other platforms, including Windows .NET 
 * and Linux Mono.Net frameworks, to supplement the current Android and HTML5 based clients. 
 * The OpenTele2 project is open source under the Apache 2.0 license - and thus this wrapper falls under the 
 * license to be fully compatiable. Creating alternative user interfaces for both patients and clinicans is 
 * important, as well as for integrating with third party medical devices and systems. Thus, we are also working 
 * on HL7 integration (FHIR and more).
 * The provided solution include a source project for data transfer objects called "OpenTele.DTO". 
 * This builds to an assembly (dll) called "OpenTele.DTO.dll". This dll may be shared across
 * different layers (GUI, BLL, DAL) but may also be used for creating distributed wrappers, e.g. HL7 FHIR based
 * wrappers. Basically, use this for domain understanding. However, OpenTeleNet.API also allows for raw JSON style  * communication as an alternative. Thus, most methods exists in both a raw JSON format, as well as an 
 * object oriented (data transfer object) approach.
 * The core of the API is the OpenTeleNet.API project and the resulting assembly "OpenTeleNet.API.dll".
 * Also in the API you can find the OpenTeleConsoleDemo project - which will provide an executable that will demo
 * how to use the API. The OpenTeleConsoleDemo project uses the two dlls "OpenTele.DTO.dll" and
 * "OpenTeleNet.API.dll" to work. 
 * For more information on OpenTele2 - please visit: http://4s-online.dk/wiki/doku.php?id=opentele:overview
 * The authors retain all rights to use the provided code and concepts for other purposes, both acadmic,  
 * teching and research, and commercial use.
 * In case of questions, please contact Stefan Wagner at sw@eng.au.dk
 * 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OpenTeleNet.API;
using OpenTele.DTO;

// A demo application to show case how it works
namespace OpenTele.GUI
{
    class Program
    {
        private static string username = "nancyann";
        private static string password = "abcd1234";
        private static string server = "http://opentele-citizen.4s-online.dk/";

        /// <summary>
        /// Will activate a range of demo web service calls to an OpenTele server.
        /// Author: Stefan Wagner
        /// </summary>
        /// <param name="args">none</param>
        static void Main(string[] args)
        {
            //First - let us create a facade for the OpenTeleNetWrapperFacade API (SW)   
            var openTeleFacade = new OpenTeleNetWrapperFacade("http://opentele-citizen.4s-online.dk/");

            //Now, let us login and get a list of questionnaires (= menu items in Android and HTML5 app) (SW)
            var list = openTeleFacade.Login(username, password) ;

            //Let us iterate through the menu items in the questionnaire (SW)
            foreach (var menuitem in list.questionnaires)
            {
                Console.WriteLine("Menu Item: " + menuitem.name + " " + menuitem.id);
            }

            //Now, let us get the measurement types of the system (SW)
            var measurements = openTeleFacade.GetMeasurementList(username, password);

            //Let us iterate through the menu items in the questionnaire (SW)
            foreach (var measurement in measurements.measurements)
            {
                if (measurement.name.Length > 2)
                    Console.WriteLine("Measurement types: " + measurement.name);
            }

            //Now, let us get the messages of the system (SW)
            var messages = openTeleFacade.GetMessageList(username, password);

            //Let us iterate through measurements (SW)
            foreach (var mess in messages.messages)
            {
                Console.WriteLine("Message: From: " + mess.from.name + " To: " + mess.to.name + " Heading " + mess.title+" Content: "+mess.text+ " Send Date: "+mess.sendDate);
            }

            //Now, let us get the bp measurements of the system (SW)
            var bp = openTeleFacade.GetBloodPressureMeasuremsents(username, password);

            //Let us iterate through measurements (SW)
            foreach (var measurement in bp.measurements)
            {
                Console.WriteLine("BP measurements: " + measurement.measurement.systolic+ 
                    "/"+ measurement.measurement.diastolic+ " mmHg "+measurement.measurement);
            }

            //So let us send a blood sugar measurement value (blood sugar = 4 - measured before meal)
            var send = openTeleFacade.postQuestionnaireBloodSugarMeasurement("true", "3", username, password);

            //Now, let us get the blood sugar measurement types of the system (SW)
            var bs = openTeleFacade.GetBloodSugarMeasurements(username, password);

            //Let us iterate through the measurements (SW)
            foreach (var measurement in bs.measurements)
            {
                Console.WriteLine("Blood Sugar: " + measurement.name + " value: " + 
                    measurement.measurement.value+ " Is it after meal?: "+ 
                    measurement.measurement.isAfterMeal+ " Time stamp: "+measurement.timestamp);
            }

            //Now, let us get the weight measurement types of the system (SW)
            var wro = openTeleFacade.GetWeightMeasurements(username, password);

            //Let us iterate through the measurements (SW)
            foreach (var measurement in wro.measurements)
            {
                Console.WriteLine("Weight: " + measurement.measurement + " at: " + measurement.timestamp);
            }

            //Lets us creat a pulse and a saturation measurement (e.g. from a Nonin)
            var sendsat = 
                openTeleFacade.postQuestionnaireSaturationAndPulseMeasurement("92", "67", username, password);
            //Now, let us get the saturation measurement types of the system (SW)
            var sat = openTeleFacade.GetSaturationMeasurement(username, password);

            //Let us iterate through the measurements (SW)
            foreach (var measurement in sat.measurements)
            {
                Console.WriteLine("Saturation: " + measurement.measurement + " at: " + measurement.timestamp);
            }

            //Now, let us get the pulse measurement types of the system (SW)
            var pulse = openTeleFacade.GetPulseMeasurement(username, password);

            //Let us iterate through the measurements (SW)
            foreach (var measurement in pulse.measurements)
            {
                Console.WriteLine("Pulse: " + measurement.measurement + " at: " + measurement.timestamp);
            }

            //So let us send a blood sugar measurement value (blood sugar = 4 - measured before meal)
            send = openTeleFacade.postQuestionnaireFEV1Measurement("5", username, password);


            //Now, let us get the weight measurement types of the system (SW)
            var fev1 = openTeleFacade.GetFEV1Measurements(username, password);

            //Let us iterate through the measurements (SW)
            foreach (var measurement in fev1.measurements)
            {
                Console.WriteLine("FEV1: " + measurement.measurement + " at: " + measurement.timestamp);
            }
            Console.WriteLine("Press Any Key to Continue");
            Console.ReadLine();

        }

    }
}
