﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using OpenTele.DTO;
using OpenTeleNet.API;
using HtmlAgilityPack;
//using OpenTeleNet;

//using Hl7.Fhir.Model;
//using Hl7.Fhir.Serialization;

namespace OpenTeleFHIR
{

    enum BasicType
    {
        Weight,
        Pulse,
    }

    [ServiceContract]
    [XmlSerializerFormatAttribute]
    class FHIRService
    {
        private OpenTeleNetWrapperFacade facade;

        FHIRService()
        {
            facade = new OpenTeleNetWrapperFacade("http://opentele-citizen.4s-online.dk/");
        }

        [WebGet(UriTemplate = "Patient/weight", BodyStyle = WebMessageBodyStyle.Bare)]
        [OperationContract]
        FhirBundle GetWeightMeasurements()
        {
            BasicMeasurementRootobject weights;

            weights = facade.GetWeightMeasurements("nancyann", "abcd1234");

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";

            FhirBundle b = CreateBundleBasicMeasurements(weights);
            return b;
        }

        private FhirPatient CreatePatient()
        {
            FhirPatient patient = new FhirPatient();
            Text text = new Text();
            text.status = new Status { value = "generated" };
            patient.text = text;

            FhirIdentifier identifier = new FhirIdentifier();
            identifier.system = new FhirSystem { value = "http://hl7.org/fhir/sid/us-ssn" };
            Type type = new Type { coding = new Coding { code = new Code { value = "SSN" } } };
            identifier.type = type;
            identifier.use = new Use { value = "official" };
            identifier.value = new Value { value = "1" };
            patient.identifier = identifier;
            patient.active = new Active { value = "true" };
            Name name = new Name();
            name.use = new Use { value = "official" };
            name.family = new Family { value = "Hobbs" };
            name.given = new Given { value = "Harley" };
            patient.name = name;
            Telecom telecom = new Telecom();
            telecom.system = new FhirSystem { value = "phone" };
            telecom.use = new Use { value = "home" };
            telecom.value = new Value { value = "077 8169 8899" };
            patient.telecom = telecom;
            patient.gender = new Gender { value = "male" };
            patient.birthDate = new BirthDate { value = "1966-06-07" };
            patient.deceasedBoolean = new DeceasedBoolean { value = "false" };

            return patient;
        }

        public FhirBundle CreateBundleBasicMeasurements(BasicMeasurementRootobject measurements)
        {

            FhirBundle bundle = new FhirBundle();
            bundle.id = new BundleId { value = "example-profile" };
            bundle.meta = new BundleMeta { lastUpdated = new BundleLastUpdated { value = (DateTime.Now).ToString() } };
            bundle.type = new BundleType { value = "collection" };
            int length = measurements.measurements.Length;
            for (int i = 0; i < length; i++)
            {

                Entry entry = new Entry();
                BundleObservation observation = new BundleObservation();
                observation.category = new Category
                {
                    coding = new BundleCoding
                    {
                        system = new BundleSystem { value = "http://hl7.org/fhir/observation-category" },
                        code = new BundleCode { value = "vital-signs", valueSpecified = true, codingSpecified = false },
                        display = new BundleDisplay { value = "Vital Signs" }
                    }
                };

                observation.code = new BundleCode
                {
                    codingSpecified = true,
                    valueSpecified = false,
                    coding = new BundleCoding
                    {
                        system = new BundleSystem { value = "http://loinc.org" },
                        code = new BundleCode { value = "3141-9", valueSpecified = true, codingSpecified = false },
                        display = new BundleDisplay { value = measurements.type + " Measured" }
                    }
                };

                float f = measurements.measurements[i].measurement;
                String value = f.ToString(System.Globalization.CultureInfo.GetCultureInfo("en-US"));

                observation.valueQuantity = new ValueQuantity
                {
                    value = new BundleValue { value = value },
                    unit = new Unit { value = measurements.unit },
                    system = new BundleSystem { value = "http://unitsofmeasure.org" }
                };

                entry.resource = new BundleResource { observation = observation };
                bundle.Add(entry);
            }

            return bundle;
        }

        public void Scrape()
        {
            HtmlWeb htmlWeb = new HtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load("\\stamdata.html");

            IEnumerable<HtmlNode> spans = htmlDocument.DocumentNode.Descendants("span").Where(x => x.Attributes.Contains("id"));
            foreach (var span in spans)
            {
                Console.WriteLine(string.Format("Span id={0}, Span text={1}", span.Attributes["id"].Value, span.InnerText));
            }
        }
    }

    [XmlRootAttribute("Bundle", Namespace = "http://hl7.org/fhir",
        IsNullable = false)]
    public class FhirBundle : List<Entry>
    {
        [XmlElement("id")]
        public BundleId id;

        [XmlElement("meta")]
        //[XmlIgnore]
        public BundleMeta meta;

        [XmlElement("type")]
        public BundleType type;

        [XmlIgnore]
        public List<Entry> entries;
    }

    public class BundleId
    {
        [XmlAttribute]
        public String value;
    }

    public class BundleMeta
    {
        [XmlElement("lastUpdated")]
        public BundleLastUpdated lastUpdated;
    }

    public class BundleLastUpdated
    {
        [XmlAttribute]
        public String value;
    }

    public class BundleType
    {
        [XmlAttribute]
        public String value;
    }

    public class Entry
    {
        [XmlElement("resource")]
        public BundleResource resource;
    }

    public class BundleResource
    {
        [XmlElement("Observation")]
        public BundleObservation observation;
    }

    public class BundleObservation
    {
        [XmlElement("text")]
        public BundleText text;

        [XmlElement]
        public Category category;

        [XmlElement]
        public BundleCode code;

        [XmlElement]
        public ValueQuantity valueQuantity;
    }

    public class BundleText
    {
    }

    public class Category
    {
        [XmlElement("coding")]
        public BundleCoding coding;
    }

    public class BundleCoding
    {
        public BundleSystem system;
        public BundleCode code;
        public BundleDisplay display;
    }

    public class BundleSystem
    {
        [XmlAttribute]
        public String value;
    }

    public class BundleCode
    {
        [XmlElement("coding")]
        public BundleCoding coding;

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool codingSpecified;

        [XmlAttribute]
        public String value;

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool valueSpecified;
    }

    public class BundleCodeComplex
    {
        [XmlAttribute]
        public String value;
    }

    public class BundleDisplay
    {
        [XmlAttribute]
        public String value;
    }

    public class ValueQuantity
    {
        [XmlElement("value")]
        public BundleValue value;

        [XmlElement]
        public Unit unit;

        [XmlElement("system")]
        public BundleSystem system;

        [XmlElement("code")]
        public BundleCode code;
    }

    public class Unit
    {
        [XmlAttribute]
        public String value;
    }

    public class BundleValue
    {
        [XmlAttribute]
        public String value;
    }

    [XmlRootAttribute("Patient", Namespace = "http://hl7.org/fhir",
        IsNullable = false)]
    public class FhirPatient
    {
        [XmlElement]
        public Text text;

        [XmlElement("identifier")]
        //[XmlIgnore]
        public FhirIdentifier identifier;

        [XmlElement]
        public Active active;

        [XmlElement]
        public Name name;

        [XmlElement]
        public Telecom telecom;

        [XmlElement]
        public Gender gender;

        [XmlElement]
        public BirthDate birthDate;

        [XmlElement]
        public DeceasedBoolean deceasedBoolean;

        //[XmlElement("address")]
        [XmlElement]
        public FhirAddress address;

        [XmlElement]
        public CareProvider careProvider;
    }

    public class Text
    {
        [XmlElement]
        public Status status;
    }

    public class Status
    {
        [XmlAttribute]
        public String value;
    }

    public class FhirIdentifier
    {
        [XmlElement]
        public Use use;

        [XmlElement]
        public Type type;

        [XmlElement]
        public FhirSystem system;

        [XmlElement]
        public Value value;
    }

    public class Use
    {
        [XmlAttribute]
        public string value;
    }

    public class Type
    {
        //[XmlElement]
        public Coding coding;
    }

    public class Coding
    {
        //[XmlElement]
        public Code code;
    }

    public class Code
    {
        [XmlAttribute]
        public string value;
    }

    public class FhirSystem
    {
        [XmlAttribute]
        public string value;
    }

    public class Value
    {
        [XmlAttribute]
        public string value;
    }

    public class Name
    {
        [XmlElement]
        public Use use;
        [XmlElement]
        public Family family;
        [XmlElement]
        public Given given;
    }

    public class Family
    {
        [XmlAttribute]
        public String value;
    }

    public class Given
    {
        [XmlAttribute]
        public String value;
    }

    public class Telecom
    {
        [XmlElement]
        public FhirSystem system;
        [XmlElement]
        public Value value;
        [XmlElement]
        public Use use;
    }

    public class FhirAddress
    {
        [XmlElement("text")]
        public AddressText text;
        [XmlElement]
        public PostalCode postalCode;
    }

    public class AddressText
    {
        [XmlAttribute]
        public String value;
    }

    public class PostalCode
    {
        [XmlAttribute]
        public String value;
    }

    public class CareProvider
    {
        [XmlElement]
        public Reference reference;
    }

    public class Reference
    {
        [XmlAttribute]
        public String value;
    }

    public class Gender
    {
        [XmlAttribute]
        public String value;
    }

    public class DeceasedBoolean
    {
        [XmlAttribute]
        public String value;
    }

    public class BirthDate
    {
        [XmlAttribute]
        public String value;
    }

    public class Active
    {
        [XmlAttribute]
        public String value;
    }
}